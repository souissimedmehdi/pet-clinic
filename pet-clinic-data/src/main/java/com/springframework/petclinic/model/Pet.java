package com.springframework.petclinic.model;

import java.time.LocalDate;

public class Pet extends BaseEntity {

    private PetType petType;
    private Owner owner;
    private LocalDate birdhDate;

    public PetType getPetType() {
        return petType;
    }

    public void setPetType(PetType petType) {
        this.petType = petType;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public LocalDate getBirdhDate() {
        return birdhDate;
    }

    public void setBirdhDate(LocalDate birdhDate) {
        this.birdhDate = birdhDate;
    }
}
